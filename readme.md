# ML Case: Predict rating of reviews


## Requirements
- docker

### Python packages
- lxml
- requests
- json
- mlflow (only needed for training, or just comment out in code)
- sklearn
- pandas
- joblib

## Components
---
- scraper.py - Scrape reviews from trustpilot.
- data_preprocessing.py - Functions to process data for ml.
- ml_functions.py - Functions for logging and printing
- train_classifier_2class.py - Trains classifier to predict of a review is positive or not.
- train_classifier_5class.py - Trains classifier to predict rating of review (1 to 5 stars).
- flask - API with trained model.
- web - Web interface which can be used to test API.
- test_deployment.py - file to test API.
- analysis.py - scratch file to do some analysis.
- mlflow - mlflow tracking server.

## Instructions
---
All instructions assume you are in the main folder.
- cd into main folder: `cd ml-case`

You can skip scraping and training if you just want to test the model.

### Scraping
Perform this step before training.
- `python src/scraper.py`

### Training
Start mlflow server: (served at http://localhost:5000)
- `docker-compose -f mlflow/docker-compose.yml up -d`

Train 2 class classifier:
- `python src/train_classifier_2class.py`

Train 5 class classifier:
- `python src/train_classifier_5class.py`

### Testing
Run with docker:
- `docker-compose -f src/flask/docker-compose.yml up -d`

Run without docker:
- `cd src/flask`
- `python app.py`

In both cases the api is served at http://localhost:5080

To use a web interface to interact with the model: open web/index.html in a browser.

Alternatively, you can also use test_deployment.py to interact with api. Just open the file in your favorite editor.