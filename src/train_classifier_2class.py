import pandas as pd
import joblib
import data_preprocessing as prep
import ml_functions as func
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
import mlflow
import mlflow.sklearn

# Set mlflow
mlflow.set_tracking_uri("http://localhost:5000")
mlflow.set_experiment("ml-case-sentiment-analysis")

# Load dataset
df = pd.read_csv('data/rebtel_reviews.csv', delimiter="|")
# Simplify rating
df['tag'] = df['rating'].apply(prep.tag_rating)

# for now we only focus reviewbody
# These cleaning steps can be performed without poluting the testset (row independent transforms)
df['reviewBody'] = df['reviewBody'].str.lower()
df = prep.remove_punctuation(df)
df = prep.remove_html(df)
df = prep.remove_stopwords(df)

# Select features and target
X = df[['reviewBody']]
# Simple two class classification, positive or not
y = df['tag']

# Split in training and testset
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, train_size=0.8, random_state=0)

# Vectorize data
# Should be performed after splitting in train and test
X_train_vector = prep.vectorize_text(X_train['reviewBody'], training_set=True)
X_test_vector = prep.vectorize_text(X_test['reviewBody'], training_set=False)

# Train classifier
with mlflow.start_run():
    
    # tag for mlflow
    mlflow.set_tag('project', 'ml-case-sentiment-analysis' )
    
    # Set parameters, tune more parameters if time
    param_c = 1 
    # Init classifier
    clf = LogisticRegression(C = param_c, random_state=0)
    # Fit classifier
    clf.fit(X_train_vector, y_train)
    # Predict
    y_pred = clf.predict(X_test_vector)

    mlflow.log_param("classes", "2")
    mlflow.log_param("model", "Logistic Regression")
    mlflow.log_param("C", param_c)

    # print and log metrics
    func.print_metrics(y_test, y_pred)
    func.log_metrics_mlflow(y_test, y_pred)

    # Save model to mlflow
    MODEL_SAVE_PATH = "D:/Projects/ml-case/data/artifacts" + mlflow.get_artifact_uri().replace("/opt/mlflow/artifactStore", "") + "/model"
    mlflow.sklearn.log_model(clf, MODEL_SAVE_PATH)
    #mlflow.end_run()

# save model
joblib.dump(clf, 'data/logistic_regression_2class.save')