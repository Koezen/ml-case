import pandas as pd
from flask import Flask, jsonify, request
from flask_cors import CORS
import data_preprocessing as prep
import joblib

# app
app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}})

# load model
model = joblib.load('data/logistic_regression_2class.save')

# routes
@app.route('/', methods=['POST'])

def predict():
    # get data
    data = request.get_json(force=True)

    print(data)
    # convert data into dataframe
    data.update((x, [y]) for x, y in data.items())
    df = pd.DataFrame.from_dict(data)

    df['reviewBody'] = df['reviewBody'].str.lower()
    df = prep.remove_punctuation(df)
    df = prep.remove_html(df)
    df = prep.remove_stopwords(df)

    print(df)
    # Vectorize data
    X_vector = prep.vectorize_text(df['reviewBody'], training_set=False)

    # predictions
    result = model.predict(X_vector)
    print(model.predict_proba(X_vector), " ", model.predict(X_vector))

    if result[0] == 1:
        result = 'positive'
    else:
        result = 'negative'

    # send back to browser
    output = {'results': result}

    # return data
    return jsonify(results=output)

if __name__ == '__main__':
    app.run(port = 5080, debug=True)

