import mlflow
from sklearn.metrics import accuracy_score, precision_score, recall_score, confusion_matrix, f1_score

def print_metrics(y_test, y_pred):
    """ Print several metrics """
    print ("Accuracy:  %s" % (accuracy_score(y_test, y_pred)))
    print("precision: ",precision_score(y_test, y_pred, average='weighted'))
    print("recall: ",recall_score(y_test, y_pred, average='weighted'))
    print("f1: ",f1_score(y_test, y_pred, average='weighted'))
    print("confusion matrix: \n", confusion_matrix(y_test, y_pred))
    print("--------- \n \n")

def log_metrics_mlflow(y_test, y_pred):
    """ Log metrics to mlflow """
    mlflow.log_metric("Accuracy", accuracy_score(y_test, y_pred))
    mlflow.log_metric("Precision", precision_score(y_test, y_pred, average='weighted'))
    mlflow.log_metric("Recall", recall_score(y_test, y_pred, average='weighted'))
    mlflow.log_metric("f1", f1_score(y_test, y_pred, average='weighted'))
