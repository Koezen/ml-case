import pandas as pd
import string
import joblib
#from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer

def tag_rating(rating):
    """Simplify 5 star rating"""
    if rating > 3:
        return 1
    else:
        return 0

def remove_punctuation(df):
    """Remove punctuation."""
    # review: could be too much is replaced
    for i in string.punctuation:
        df['reviewBody'] = df['reviewBody'].str.replace(i, " ")

    return df

def remove_html(df):
    """Remove html tags."""
    df['reviewBody'] = df['reviewBody'].str.replace(" \n", " ")
    df['reviewBody'] = df['reviewBody'].str.replace("\n", " ")
    return df

def remove_stopwords(df):
    """Remove stopwords.
    
    Passing entire df so you only need to load stopwords once.
    :param df: Dataframe with text content
    :return: DataFrame stripped from stopwords
    """
    # Create stopword list
    # nltk list of stop words removes also the negations, so I provide a custom list
    stop = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'are', 'was', 'were', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'only', 'own', 'same', 'so', 'than', 'too', 'will', 'just', 'now']
    # Remove stopwords
    df['reviewBody'] = df['reviewBody'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop)]))
    
    return df

def vectorize_text(reviews, training_set=False):
    """ Vectorize reviews. Do this after cleaning

    :param reviews: series with reviews.
    :param training_set: boolean in order to save vectorizer during training.
    :return: collection of token counts.
    """

    if training_set == True:
        # Initialize vectorizer
        cv = CountVectorizer(binary=False, ngram_range=(1, 2))
        # Fit it with test set
        cv.fit(reviews)
        # Save vectorize for use on test set
        joblib.dump(cv, 'data/vectorizer.save')
    else:
        # Load vectorizer (should put in test if file exists)
        cv = joblib.load('data/vectorizer.save') 

    # Vectorize reviews
    X = cv.transform(reviews)

    return X

def tfidf_transform(reviews, training_set=False):
    """ tf-idf reviews. Do this after cleaning

    :param reviews: series with reviews.
    :param training_set: boolean in order to save vectorizer during training.
    :return: tf-idf'd dataframe.
    """

    if training_set == True:
        # Initialize vectorizer
        tf = TfidfTransformer()
        # Fit it with test set
        tf.fit(reviews)
        # Save vectorize for use on test set
        joblib.dump(tf, 'data/tfidf_transformer.save')
    else:
        # Load vectorizer (should put in test if file exists)
        tf = joblib.load('data/tfidf_transformer.save') 

    # Vectorize reviews
    X = tf.transform(reviews)

    return X


 





