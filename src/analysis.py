import joblib
import pandas as pd
import matplotlib.pyplot as plt

#################
# Analyse model #
#################
cv = joblib.load('data/vectorizer.save')
clf = joblib.load('data/logistic_regression_2class.save')

feature_to_coef = {
    word: coef for word, coef in zip(
        cv.get_feature_names(), clf.coef_[0]
    )
}

for best_positive in sorted(
    feature_to_coef.items(), 
    key=lambda x: x[1], 
    reverse=True)[:5]:
    print (best_positive)
       
for best_negative in sorted(
    feature_to_coef.items(), 
    key=lambda x: x[1])[:25]:
    print (best_negative)

# Show distribution of ratings
df = pd.read_csv('data/rebtel_reviews.csv', delimiter="|")
ax = df['rating'].value_counts(sort=False).plot(kind='barh')
ax.set_xlabel("Number of Samples in training Set")
ax.set_ylabel("Label")