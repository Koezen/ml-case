# https://docs.python-guide.org/scenarios/scrape/

from lxml import html
import requests
import pandas as pd
import json

def main():

    print("Started Scraping")
    
    # Define url
    base_url = 'https://www.trustpilot.com/review/www.rebtel.com?languages=en'
    # Starting page number
    page_number = 1

    # Initialize dataframe to store all reviews
    all_reviews = pd.DataFrame()

    while True:
        # construct url
        web_url = base_url + '&page=' + str(page_number)
        # Get page
        page = requests.get(web_url)

        # If there are no more pages left trustpilot will return the main url instead of the specific page.
        if page.url != web_url:
            print('Finished scraping')
            break
        else:
            # Parse web page
            tree = html.fromstring(page.content)
            # Extract reviews
            df = extract_reviews_json(tree)
            # Add reviews of current page to the other reviews
            all_reviews = pd.concat([all_reviews, df])

            # Increase page number
            page_number = page_number + 1

    # reset index
    all_reviews = all_reviews.reset_index(drop=True)
    # Save reviews to csv
    all_reviews.to_csv('data/rebtel_reviews.csv', sep='|')

def extract_reviews_json(tree):
    """ Parse JSON object for english reviews

    Somehow only works with english reviews. Even if non-english reviews are on
    the page the json only contains english reviews. I dont care for now as
    it makes the sentiment anlysis easier as there is no language indicator on the review cards.
    In base url select english language only.

    :param tree: lxml tree html content
    :return: dataframe 
    """
    # Get json with data from page (listed in head of html)
    data =  tree.xpath('//script[@data-business-unit-json-ld]')
    # Only one object. Get json string
    data = data[0].text_content()
    # Strip newlines, spaces at start and end, semicolon
    data = data.replace("\n", "").strip().rstrip(";")
    # Convert to dict and get first object as reviews are in there
    data = json.loads(data)[0]
    # Get reviews from json
    reviews = data['review']

    # Convert to dataframe
    df = pd.DataFrame(reviews)
    # Extract rating, as it also list highest and lowest of all reviews
    df['rating'] = df['reviewRating'].str['ratingValue']

    # return relevant columns
    return df[['datePublished', 'headline', 'reviewBody', 'inLanguage', 'rating']]

def extract_reviews_html(tree):
    """ Parse reviews in all languages

    There is no indicator on the reviewcard for the language.
    Best used while indicating language in base url
    
    :param tree: lxml tree html content
    :return: dataframe 
    """
    # Get titles
    review_titles = tree.xpath('//a[@class="link link--large link--dark"]')
    # Get review body
    review_content = tree.xpath('//p[@class="review-content__text"]')

    # Get stars
    reviews_info =  tree.xpath('//script[@data-initial-state="review-info"]')
 
    # Get publish date
    reviews_dates =  tree.xpath('//script[@data-initial-state="review-dates"]')

    # Convert reviews to dataframe
    reviews = pd.DataFrame()
    for i in range(0, len(review_titles)):
        review = pd.DataFrame([{
            "datePublished":json.loads(reviews_dates[i].text_content().replace('\n',''))['publishedDate'],
            "headline":review_titles[i].text_content(),
            "reviewBody":review_content[i].text_content().replace('\n','').strip(),
            "rating":json.loads(reviews_info[i].text_content().replace('\n',''))['stars']}])

        reviews = pd.concat([reviews,review])
    
    return reviews

main()
        

