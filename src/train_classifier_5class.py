import pandas as pd
import joblib
import data_preprocessing as prep
import ml_functions as func
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression, SGDClassifier
import mlflow
import mlflow.sklearn

# Set mlflow
mlflow.set_tracking_uri("http://localhost:5000")
mlflow.set_experiment("ml-case-sentiment-analysis")

# Load dataset
df = pd.read_csv('data/rebtel_reviews.csv', delimiter="|")

# for now we only focus reviewbody
df['reviewBody'] = df['reviewBody'].str.lower()
df = prep.remove_punctuation(df)
df = prep.remove_html(df)
df = prep.remove_stopwords(df)

# categorize rating
df['rating'] = df['rating'].astype('category')

# Select features and target
X = df[['reviewBody']]
y = df['rating']

# Split in training and testset
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, train_size=0.8, random_state=0)

# Vectorize data
# Should be performed after splitting in train and test
X_train_vector = prep.vectorize_text(X_train['reviewBody'], training_set=True)
X_test_vector = prep.vectorize_text(X_test['reviewBody'], training_set=False)

# tfidf
X_train_vector = prep.tfidf_transform(X_train_vector, training_set=True)
X_test_vector = prep.tfidf_transform(X_test_vector, training_set=False)

# Train classifier
with mlflow.start_run():
    
    # Set mlflow tag
    mlflow.set_tag('project', 'ml-case-sentiment-analysis' )
    # Set parameters, tune if time
    param_c = 1 
    param_penalty = 'l2'
    param_solver = 'liblinear'
    param_maxiter = 200
    
    # Initialize classifier
    #clf = SGDClassifier(loss=param_loss,penalty=param_penalty,alpha=param_alpha, max_iter=param_maxiter, learning_rate=param_lr, tol=param_tol, random_state=0)
    
    clf = LogisticRegression(C = param_c, penalty=param_penalty, solver=param_solver, max_iter=param_maxiter, random_state=0)
    # fit classifier
    clf.fit(X_train_vector, y_train)
    # Predict
    y_pred = clf.predict(X_test_vector)

    # log params to mlflow
    mlflow.log_param("classes", "5")
    mlflow.log_param("model", "Logistic Regression")
    mlflow.log_param("C", param_c)
    mlflow.log_param("penalty", param_penalty)
    mlflow.log_param("solver", param_solver)
    mlflow.log_param("maxiter", param_maxiter)

    # print and log metrics
    func.print_metrics(y_test, y_pred)
    func.log_metrics_mlflow(y_test, y_pred)
    
    # Save model to mlflow
    MODEL_SAVE_PATH = "D:/Projects/ml-case/data/artifacts" + mlflow.get_artifact_uri().replace("/opt/mlflow/artifactStore", "") + "/model"
    mlflow.sklearn.log_model(clf, MODEL_SAVE_PATH)

# save model
joblib.dump(clf, 'data/logistic_regression_5class.save')
