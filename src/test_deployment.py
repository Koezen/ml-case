import requests
import json

url = 'http://127.0.0.1:5080' 
# sample data
data = {'reviewBody': 'This is Great!'}
data = json.dumps(data)

print(requests.post(url, data).json())
