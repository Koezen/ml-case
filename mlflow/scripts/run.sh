#!/bin/sh

mlflow server \
    --backend-store-uri postgresql://mlflow:mlflow@$POSTGRES_HOST/mlflow \
    --default-artifact-root $ARTIFACT_STORE \
    --host $SERVER_HOST \
    --port $SERVER_PORT

